/* Created by Felipe Oyarzun. */

#include <stdio.h>
#include <server.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <lang.h>

/* Tamaño del buffer de lectura/escritura. */
#define BUFFER_SIZE 128

void server_name()
{
    printf("\n");
    printf("̣ @@@@@@@@@@@   @@@@@@@@@@@   @@@@@@@@@@@\n");
    printf(" @         @   @         @   @         @\n");
    printf(" @@@@   @@@@   @   @@@@@@@   @    @@@  @\n");
    printf("    @   @      @   @         @         @\n");
    printf("̣̣̣̣̣̣    @   @      @   @         @   @@@@@@@\n");
    printf("    @   @      @   @@@@@@@   @   @      \n");
    printf("    @   @      @         @   @   @      \n");
    printf("    @@@@@      @@@@@@@@@@@   @@@@@      \n");
    printf("\n");
}

/* Funcion encargada de recibir al cliente entrante y constestar sus peticiones al servidor
   hasta que el cliente sea cerrado. Funcion Bloqueante. */
void client_handler(connection_t client)
{
    char buffer[BUFFER_SIZE];

    for(;;)
    {
        bzero(buffer, BUFFER_SIZE);

        if (read(client.connection, buffer, BUFFER_SIZE) < 0)
        {
            perror("Error reading from the socket");
            close(client.connection);

            exit(5);
        }

        if (strcmp(buffer, "pid") == 0)
        {
            bzero(buffer, BUFFER_SIZE);
            sprintf(buffer, "Pid: %d", getpid());
        }
        else if(strcmp(buffer, "path") == 0)
        {
            bzero(buffer, BUFFER_SIZE);

            if (getcwd(buffer, BUFFER_SIZE) == NULL)
            {
                perror("Get CWD");
            }
        }
        else if (strcmp(buffer, "exit") == 0)
        {
            printf("Client close\n");

            exit(6);
        }
        else
        {
            bzero(buffer, BUFFER_SIZE);
            sprintf(buffer, "Command not found");
        }

        if (write(client.connection, buffer, BUFFER_SIZE) < 0)
        {
            perror("Error writing to socket");
            close(client.connection);

            exit(6);
        }
    }
}

int main(int argc, char **argv)
{
    /* Comienzo de las validaciones de parametros del programa. */
    if (argc != 3)
    {
        printf("Ussage: server [ip address] [max connections]\n");
        return 1;
    }

    if (!isValidIpAddress(argv[1]))
    {
        printf("Invalid Ip Address\n");

        return 2;
    }

    if (atoi(argv[2]) < 0)
    {
        printf("Invalid Number of Connections\n");

        return 3;
    }
    /* Fin de las validaciones de parametros del programa. */

    server_name();

    connection_t server = open_server(argv[1], 0, atoi(argv[2]));

    /* Comprobacion de la integridad de la coneccion. */
    if (server.status == SOCK_FAILED)
    {
        printf("Failed to create Socket\n");

        return 4;
    }
    else if (server.status == SOCK_BIND_FAILED)
    {
        printf("Failed to Bind Socket\n");

        return 5;
    }

    struct sockaddr_in sin;
    socklen_t len = sizeof(sin);
    getsockname(server.connection, (struct sockaddr*)&sin, &len);

    printf("Server: %s\n", argv[1]);
    printf("Port: %d\n\n", ntohs(sin.sin_port));

    /* Asigna una funcion anonima como callback dada por lambda en respuesta a la interrupcion "ctrl + c".*/
    signal(SIGINT,
        lambda(void, (),
        {
            printf("\nClose Server\n");
            close(server.connection);

            exit(0);
        }));

    /* Loop de ejecucion del servidor. */
    for (;;)
    {
        printf("Waiting for Connection...\n");
        connection_t client = accept_client(server);
        printf("Connection Stablished\n");

        /* Crea un proceso hijo encargado de tomar las peticiones del Cliente. */
        pid_t pchild = fork();
        
        if (pchild == 0)
        {
            client_handler(client);
        }
        else if (pchild < 0)
        {
            perror("Fork Error");

            exit(-1);
        }
    }

    return  0;
}
