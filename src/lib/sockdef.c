/* Created by Felipe Oyarzun. */

#include <sockdef.h>

bool isValidIpAddress(char *ip)
{
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ip, &(sa.sin_addr));

    return result != 0;
}