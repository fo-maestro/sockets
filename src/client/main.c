/* Created by Felipe Oyarzun. */

#include <stdio.h>
#include <client.h>
#include <stdlib.h>
#include <string.h>

/* Tamaño del buffer de lectura/escritura */
#define BUFFER_SIZE 128

/* Lista de comandos disponibles. */
void command_list()
{
    printf("commands:\n");
    printf("pid             get pid of the daemon process handler\n");
    printf("path            get the current path of the server program\n");
    printf("help            show this message\n");
    printf("exit            close this client\n");
    printf("\n");
}

int main(int argc, char **argv)
{
    /* Comienzo de las validaciones de parametros del programa. */
    if (argc != 3)
    {
        printf("Usage: client [ip address] [port]\n");

        return 1;
    }

    if (!isValidIpAddress(argv[1]))
    {
        printf("Invalid Ip Address\n");

        return 2;
    }

    if (atoi(argv[2]) <= 0)
    {
        printf("Invalid Port Number");

        return 3;
    }
    /* Fin de las validaciones de parametros del programa. */

    connection_t server = open_client(argv[1], atoi(argv[2]));

    if (connect_server(server) == -1)
    {
        perror("Failed to Connect");

        return 4;
    }

    char buffer[BUFFER_SIZE];
    command_list();

    /* Loop de comunicacion cliente/servidor. */
    for (;;)
    {
        bzero(buffer, BUFFER_SIZE);
        printf("client@tcp> ");
        scanf("%s", buffer);

        if (strcmp(buffer, "help") == 0)
        {
            command_list();

            continue;
        }
        else if (strcmp(buffer, "exit") == 0)
        {
            write(server.connection, buffer, BUFFER_SIZE);
            printf("Close connection\n");
            
            break;
        }

        if (write(server.connection, buffer, BUFFER_SIZE) < 0)
        {
            perror("Error writing to socket");

            exit(5);
        }

        bzero(buffer, BUFFER_SIZE);

        if (read(server.connection, buffer, BUFFER_SIZE) < 0)
        {
            perror("Error reading to socket");

            exit(6);
        }

        if (strcmp(buffer, "") == 0)
        {
            printf("Error: Broken pipeline\n");

            exit(7);
        }

        printf("%s\n", buffer);
    }

    return 0;
}
