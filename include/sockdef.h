/* Created by Felipe Oyarzun. */

#ifndef _SOCKDEF_H
#define _SOCKDEF_H

#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdbool.h>
#include <unistd.h>

/* Enum que describe los diferentes estados en el momento de la creacion de un socket. */
typedef enum _status
{
    SOCK_OK, SOCK_FAILED, SOCK_BIND_FAILED
}status_t;

/* Estructura que almacena la coneccion de un socket, su direccion y ip y su puerto asociado,
   ademas del estado al momento de su creacion. */
typedef struct _connection
{
    int connection;
    struct sockaddr_in address;
    status_t status;
}connection_t;

/* Devuelve true si la direccion ip ingresada corresponde a una ip valida. */
bool isValidIpAddress(char *ip);

#endif /* _SOCKDEF_H */
